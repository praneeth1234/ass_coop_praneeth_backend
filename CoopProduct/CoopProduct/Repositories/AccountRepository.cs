﻿using CoopProduct.IRepositories;
using CoopProduct.Models;
using CoopProduct.Oracle;
using Dapper;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CoopProduct.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private IConfiguration configuration;
        public AccountRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public async Task<bool> Delete(string Id)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, Id);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "ACC_DELE";
                    (await SqlMapper.QueryAsync(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }

        public async Task<List<Account>> GetAccount()
        {
            List<Account> result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PRODCURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "ACC_ALL";
                    result = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure)).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        public async Task<Account> GetGetAccountById(string Id)
        {
            Account result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, Id);
                dyParam.Add("PROD_DETAILS_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "ACC_BYID";
                    result = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }

        public async Task<bool> AddAccount(Account account)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                account.Id = Guid.NewGuid().ToString();
                dyParam.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, account.Id);
                dyParam.Add("ACC_NAME", OracleDbType.Varchar2, ParameterDirection.Input, account.Name);
                dyParam.Add("ACC_BALANCE", OracleDbType.Double, ParameterDirection.Input, account.Balance);
                dyParam.Add("ACC_BRANCH", OracleDbType.Varchar2, ParameterDirection.Input, account.Branch);
                dyParam.Add("ACC_OWNER", OracleDbType.Varchar2, ParameterDirection.Input, account.Owner);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "ACC_ADD";
                    await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        public async Task<bool> UpdateAccount(Account account)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, account.Id);
                dyParam.Add("ACC_NAME", OracleDbType.Varchar2, ParameterDirection.Input, account.Name);
                dyParam.Add("ACC_BALANCE", OracleDbType.Double, ParameterDirection.Input, account.Balance);
                dyParam.Add("ACC_BRANCH", OracleDbType.Varchar2, ParameterDirection.Input, account.Branch);
                dyParam.Add("ACC_OWNER", OracleDbType.Varchar2, ParameterDirection.Input, account.Owner);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "ACC_UPDATE";
                    await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        public IDbConnection GetConnection()
        {
            var connectionString = configuration.GetSection("ConnectionStrings").GetSection("ProductConnection").Value;
            var conn = new OracleConnection(connectionString);
            return conn;
        }

    }
}
