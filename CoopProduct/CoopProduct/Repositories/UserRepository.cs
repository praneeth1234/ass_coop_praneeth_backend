﻿using CoopProduct.IRepositories;
using CoopProduct.Iservices;
using CoopProduct.Models;
using CoopProduct.Oracle;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CoopProduct.Repositories
{
    public class UserRepository : IUserRepository
    {
        private IConfiguration configuration;

        public UserRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }
        public async Task<User> Authenticate(string username, string password)
        {
            User result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("USERNAME_GET", OracleDbType.Varchar2, ParameterDirection.Input, username);
                dyParam.Add("PASSWORD_GET", OracleDbType.Varchar2, ParameterDirection.Input, password);
                dyParam.Add("USER_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var query = "ACC_LOGIN";

                    result = (await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (result == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING");
            var tokenDescripter = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.Name,result.Id.ToString()),
                    new Claim(ClaimTypes.Role, result.Role)
                }),

                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescripter);
            result.Token = tokenHandler.WriteToken(token);
            result.Password = null;
            return result;
        }

        public async Task<List<User>> GetUsers()
        {
            List<User> result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("USERCURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "US_SERACHALLPROCEDUAR";
                    result = (await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure)).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        public async Task<User> GetUserById(string Id)
        {
            User result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("U_Id", OracleDbType.Int32, ParameterDirection.Input, Id);
                dyParam.Add("US_DETAILS_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "US_SEARCHIDPROCEDUAR";
                    result = (await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        public async Task<bool> AddUser(User User)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();

                dyParam.Add("U_Id", OracleDbType.Int32, ParameterDirection.Input, User.Id);

                dyParam.Add("U_UserName", OracleDbType.Varchar2, ParameterDirection.Input, User.UserName);
                dyParam.Add("U_Password", OracleDbType.Varchar2, ParameterDirection.Input, User.Password);
                dyParam.Add("U_Role", OracleDbType.Varchar2, ParameterDirection.Input, User.Role);

                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "US_ADDPROCEDUAR";
                    await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        public async Task<bool> UpdateUser(User User)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("U_Id", OracleDbType.Int32, ParameterDirection.Input, User.Id);
                // dyParam.Add("U_FirstName", OracleDbType.Varchar2, ParameterDirection.Input, User.FirstName);
                // dyParam.Add("U_LastName", OracleDbType.Varchar2, ParameterDirection.Input, User.LastName);
                dyParam.Add("U_UserName", OracleDbType.Varchar2, ParameterDirection.Input, User.UserName);
                dyParam.Add("U_Password", OracleDbType.Varchar2, ParameterDirection.Input, User.Password);
                dyParam.Add("U_Role", OracleDbType.Varchar2, ParameterDirection.Input, User.Role);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "US_UPDATEPROCEDUAR";
                    await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        public async Task<bool> DeleteUser(string Id)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("U_Id", OracleDbType.Int32, ParameterDirection.Input, Id);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "US_DELETEPROCEDUAR";
                    (await SqlMapper.QueryAsync(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        public IDbConnection GetConnection()
        {
            var connectionString = configuration.GetSection("ConnectionStrings").GetSection("ProductConnection").Value;
            var conn = new OracleConnection(connectionString);
            return conn;
        }
    }
}
