﻿using CoopProduct.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoopProduct.Iservices
{
    public interface IAccountService
    {
        Task<List<Account>> GetAccount();
        Task<Account> GetAccountById(string Id);
        Task<bool> AddAccount(Account account);
        Task<bool> UpdateAccount(Account account);
        Task<bool> Delete(string Id);
    }
}
