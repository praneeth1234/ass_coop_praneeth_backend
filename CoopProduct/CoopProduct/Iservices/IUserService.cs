﻿using CoopProduct.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoopProduct.Iservices
{
    public interface IUserService
    {
        //IEnumerable<User> GetAll();
        Task<User> Authenticate(string username, string password);
        Task<List<User>> GetUsers();
        Task<User> GetUserById(string Id);
        Task<bool> AddUser(User user);
        Task<bool> UpdateUser(User user);
        Task<bool> DeleteUser(string Id);


    }
}
