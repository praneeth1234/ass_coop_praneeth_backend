﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CoopProduct.IRepositories;
using CoopProduct.Iservices;
using CoopProduct.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CoopProduct.Iservices
{
    public class UserService : IUserService
    {
        IConfiguration configuration;
        IUserRepository userRepository;

        public UserService(IConfiguration _configuration, IUserRepository _userRepository)
        {
            configuration = _configuration;
            userRepository = _userRepository;
        }
        public async Task<bool> AddUser(User User)
        {
            var adduser = await userRepository.AddUser(User);
            return adduser;
        }
        public async Task<User> Authenticate(string username, string password)
        {
            var loguser = await userRepository.Authenticate(username, password);
            return loguser;
        }
        public async Task<bool> DeleteUser(string Id)
        {
            var deleteuser = await userRepository.DeleteUser(Id);
            return deleteuser;
        }
        public async Task<User> GetUserById(string Id)
        {
            var getid = await userRepository.GetUserById(Id);
            return getid;
        }
        public async Task<List<User>> GetUsers()
        {
            var user = await userRepository.GetUsers();
            return user;
        }

        public async Task<bool> UpdateUser(User User)
        {
            var updateuser = await userRepository.UpdateUser(User);
            return updateuser;
        }
    }
}
