﻿using CoopProduct.IRepositories;
using CoopProduct.Iservices;
using CoopProduct.Models;
using CoopProduct.Oracle;
using Dapper;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CoopProduct.Iservices
{
    class AccountService : IAccountService
    {
        IConfiguration configuration;
        IAccountRepository accountRepository;
        public AccountService(IConfiguration _configuration, IAccountRepository _accountRepository)
        {
            configuration = _configuration;
            accountRepository = _accountRepository;
        }
        public async Task<bool> Delete(string Id)
        {
            var account1 = await accountRepository.Delete(Id);
            return account1;
        }

        public async Task<List<Account>> GetAccount()
        {
            var account1 = await accountRepository.GetAccount();
            return account1;
        }

        public async Task<Account> GetAccountById(string Id)
        {

            var account1 = await accountRepository.GetGetAccountById(Id);
            return account1;
        }

        public async Task<bool> AddAccount(Account account)
        {
            var account1 = await accountRepository.AddAccount(account);
            return account1;
        }
        public async Task<bool> UpdateAccount(Account account)
        {
            var account1 = await accountRepository.UpdateAccount(account);
            return account1;
        }
        public IDbConnection GetConnection()
        {
            var connectionString = configuration.GetSection("ConnectionStrings").GetSection("ProductConnection").Value;
            var conn = new OracleConnection(connectionString);
            return conn;
        }
    }
}
