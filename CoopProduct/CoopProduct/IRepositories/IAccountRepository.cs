﻿using CoopProduct.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoopProduct.IRepositories
{
    public interface IAccountRepository
    {
        Task<List<Account>> GetAccount();
        Task<Account> GetGetAccountById(string Id);
        Task<bool> AddAccount(Account account);
        Task<bool> UpdateAccount(Account account);
        Task<bool> Delete(string Id);
    }
}
