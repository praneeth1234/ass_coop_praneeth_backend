﻿using CoopProduct.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoopProduct.IRepositories
{
    public interface IUserRepository
    {
        // IEnumerable<User> GetAll();
        Task<User> Authenticate(string username, string password);
        Task<List<User>> GetUsers();
        Task<User> GetUserById(string Id);
        Task<bool> AddUser(User product);
        Task<bool> UpdateUser(User product);
        Task<bool> DeleteUser(string Id);
    }
}
