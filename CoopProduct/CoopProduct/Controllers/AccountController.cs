﻿using System.Threading.Tasks;
using CoopProduct.Iservices;
using CoopProduct.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoopProduct.Controllers
{
    [Authorize]
    [Route("v1/accounts")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        //select all account details,both managers and clerks allowed
        [HttpGet]
        public async Task<IActionResult> GetAccounts()
        {
            var account = await _accountService.GetAccount();
            return Ok(account);
        }
        [Route("{Id}")]
        [HttpGet]
        public async Task<IActionResult> GetAccountsById(string Id)
        {
            var account = await _accountService.GetAccountById(Id);
            if (account != null)
            {
                return Ok(account);
            }
            else
            {
                return NotFound("account not found with given id");
            }
        }
        //insert account details
        [HttpPost]
        public async Task<IActionResult> AddAccounts([FromBody]Account account)
        {
            var account1 = await _accountService.AddAccount(account);
            return Ok(account1);
        }
        //update acccount details,can update only managers
        [Authorize(Policy = "ManagerOnly")]
        [HttpPut]
        public async Task<IActionResult> UpdateAccounts([FromBody]Account account)
        {
            var updatedaccount = await _accountService.UpdateAccount(account);
            if (updatedaccount == false)
            {
                return NotFound("No account found with given id");
            }
            return Ok("Successfully updated");
        }
        [Authorize(Policy = "ManagerOnly")]
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAccounts(string Id)
        {
            var accountdelet = await _accountService.Delete(Id);
            if (accountdelet == false)
            {
                return NotFound("No account found with given id");
            }
            return Ok("Deleted Successfully");
        }
    }
}