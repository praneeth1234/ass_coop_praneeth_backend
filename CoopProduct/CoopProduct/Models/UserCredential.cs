﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoopProduct.Models
{
    public class UserCredential
    {
        [StringLength(10, MinimumLength = 3)]
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
